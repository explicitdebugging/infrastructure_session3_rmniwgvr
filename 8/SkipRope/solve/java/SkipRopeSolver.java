import java.io.FileReader;
import java.io.FileWriter;

import java.util.List;
import java.util.ArrayList;

import org.topcoder.TopcoderReader;
import org.topcoder.TopcoderWriter;
import org.topcoder.TypeRef;

public class SkipRopeSolver {
    public static void main(String[] args) {
    try {
        TopcoderReader reader = new TopcoderReader(new FileReader(args[0]));
        List<Integer> candidatesBoxed = (List<Integer>) reader.next(new TypeRef<List<Integer>>(){}.getType());
        int[] candidates = new int[candidatesBoxed.size()];
        for (int _i = 0; _i < candidatesBoxed.size(); ++_i)
            candidates[_i] = candidatesBoxed.get(_i);
        reader.next();
        
        int height = (Integer) reader.next(Integer.class);
        reader.close();

        SkipRope solver = new SkipRope();
        TopcoderWriter writer = new TopcoderWriter(new FileWriter(args[1]));
        int[] result = solver.partners(candidates, height);
        List<Integer> resultBoxed = new ArrayList<Integer>();
        for (int _i = 0; _i < result.length; ++_i) {
            resultBoxed.add(result[_i]);
        }
        writer.write(resultBoxed);
        writer.close();
    } catch (Exception err) {
        err.printStackTrace(System.err);
    }
    }
}
