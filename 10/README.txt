*STEPS*

1) Make sure you have your favorite IDE installed.

2) Read the problem statement in folder prob/[problem-name].html

3) Open and inspect the buggy code: cd [problem-name]/solve/java e.g. cd CardCount/solve/java

4) run the test cases to see which one passes or fails using the command "make demo" and "make sys". Quotation marks are for emphasis

5) Copy and paste the following link to your browser and answer the questions:

https://docs.google.com/forms/d/e/1FAIpQLSesxTwEx-zv9TEbu1JMP7ss81fCOXpB_AsaiFRWt7h1svTSbA/viewform?entry.2141507083=RMNIWGVR&entry.1083320178

*PROBLEM STATEMENT UNDERSTANDING*
=to read problem statement go to folder prob/[problem-name].html for example prob/CardCount.html


*COPYING/IMPORTING CODE INTO YOUR IDE*
=If you are more comfortable with using IDE, You may copy/import the buggy code into your IDE.
=After correcting code you may copy it back to infrastructure to test.


*RUN TEST CASES*
=there are two sets of test cases : demo and sys
==demo runs the test cases that are listed in the problem statement file. To run do: "make demo"
==sys are additional test cases. To run do: "make sys" to show only failing sys test cases or "make sysv" to show both failing and passing sys test cases
=a successful fix must pass all test cases


*DEBUGGING*
=If any of the system test cases fail, you will receive a message in the command line. The message has the structure:
	[exec] Here are a few failed case for your debugging purposes
    	[exec] Case 5:
             [exec]     Input: <12,
             [exec] 88,
             [exec] 14>
             [exec]     Expected: <72.5>
             [exec]     Received: <155.0>

in this test case, the input to the function were integers 12, 88 and 14; The output from the function was 155.0 but the expected output is 72.5. Hence this function did not pass all test cases.
