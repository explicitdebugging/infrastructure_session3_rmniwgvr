# [TheArithmeticProgression]

## Statement
A triple (x, y, z) is called an arithmetic progression if the equality y - x = z - y holds.

You are given three ints *a*, *b* and *c*. Your goal is to change the triple (*a*, *b*, *c*) into an arithmetic progression.
You are only allowed to change one of the three numbers.
The change must proceed as follows:
First, you choose a non-negative real (not necessarily integer) number r.
Then, you either add r to one of the three given numbers, or you subtract r from one of them. Return the minimum value of r which allows you to create an arithmetic progression.

## Definitions
- *Class*: `TheArithmeticProgression`
- *Method*: `minimumChange`
- *Parameters*: `int, int, int`
- *Returns*: `double`
- *Method signature*: `double minimumChange(int a, int b, int c)`

## Constraints
- *a* will be between 0 and 1000, inclusive.
- *b* will be between 0 and 1000, inclusive.
- *c* will be between 0 and 1000, inclusive.

## Examples
### Example 1
#### Input
<c>0,<br />1,<br />2</c>
#### Output
<c>0.0</c>
#### Reason
The triple (0, 1, 2) is an arithmetic progression. Thus, you can choose r = 0.0 and add or subtract it from any of the given numbers without changing the triple.

### Example 2
#### Input
<c>0,<br />2,<br />1</c>
#### Output
<c>1.5</c>
#### Reason
Note that while (0, 1, 2) is an arithmetic progression, you cannot rearrange the numbers within the triple. You can choose r = 1.5 and subtract it from *b*, obtaining the triple (0, 0.5, 1).

### Example 3
#### Input
<c>3,<br />2,<br />1</c>
#### Output
<c>0.0</c>
### Example 4
#### Input
<c>4,<br />4,<br />8</c>
#### Output
<c>2.0</c>

