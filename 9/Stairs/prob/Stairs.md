# [Stairs]


## Statement
A set of stairs consists of risers (the vertical parts of the stairs) and
treads (the horizontal parts that you walk on).  The stairs alternate
treads and risers, starting and ending with a riser as shown below.

A set of stairs with two treads would have three risers and would look 
similar to this picture:

+........
                |
            +---+
            |
        +---+
        |
........+

We have the following 
requirements for a set of stairs:

all risers must have the same integer heightall treads must have the same integer widtheach riser must be less than or equal to *maxHeight*each tread must be greater than or equal to *minWidth*

The *totalWidth* of the stairs is the sum of all the tread widths, while the
*totalHeight* of the stairs is the sum of all the riser heights.  The stairs start
with a riser and end with a riser.

Create a class Stairs that contains a method designs that takes as input four
ints:  *maxHeight*, *minWidth*, *totalHeight*, *totalWidth*. It returns the number
of different designs that meet the design criteria.

## Definitions
- *Class*: `Stairs`
- *Method*: `designs`
- *Parameters*: `int, int, int, int`
- *Returns*: `int`
- *Method signature*: `int designs(int maxHeight, int minWidth, int totalHeight, int totalWidth)`

## Constraints
- *maxHeight*, *minWidth*, *totalHeight*, and *totalWidth* will be between 1 and 1000 inclusive

## Examples
### Example 1
#### Input
<c>22,<br />25,<br />100,<br />100</c>
#### Output
<c>1</c>
#### Reason
The only design is to have each riser be 20, each tread be 25.

### Example 2
#### Input
<c>25,<br />25,<br />60,<br />100</c>
#### Output
<c>2</c>
#### Reason
We could have riser height 12 with tread width 25, or we could have
   riser height 20, tread width 50. The design with just
   one tread of width 100 would force each riser to be 30 which exceeds the
   specified *maxHeight*, and a design with 6 risers of height 10 would 
   result in treads of width 20 which is smaller than the specified *minWidth*.

### Example 3
#### Input
<c>1000,<br />1,<br />600,<br />600</c>
#### Output
<c>6</c>
#### Reason
There are six different designs. The one with the biggest steps has just one tread of size 600, and two risers of size 300. The one with the smallest steps has 24 treads, each of width 25, and its 25 risers each have a height of 24.


