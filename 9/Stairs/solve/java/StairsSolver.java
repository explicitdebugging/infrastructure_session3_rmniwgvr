import java.io.FileReader;
import java.io.FileWriter;

import java.util.List;
import java.util.ArrayList;

import org.topcoder.TopcoderReader;
import org.topcoder.TopcoderWriter;
import org.topcoder.TypeRef;

public class StairsSolver {
    public static void main(String[] args) {
    try {
        TopcoderReader reader = new TopcoderReader(new FileReader(args[0]));
        int maxHeight = (Integer) reader.next(Integer.class);
        reader.next();
        
        int minWidth = (Integer) reader.next(Integer.class);
        reader.next();
        
        int totalHeight = (Integer) reader.next(Integer.class);
        reader.next();
        
        int totalWidth = (Integer) reader.next(Integer.class);
        reader.close();

        Stairs solver = new Stairs();
        TopcoderWriter writer = new TopcoderWriter(new FileWriter(args[1]));
        writer.write(solver.designs(maxHeight, minWidth, totalHeight, totalWidth));
        writer.close();
    } catch (Exception err) {
        err.printStackTrace(System.err);
    }
    }
}
