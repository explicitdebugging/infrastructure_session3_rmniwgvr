# [Quipu]

## Statement
The Incas used a sophisticated system of record keeping consisting of bundles of knotted cords.
Such a bundle of cords is called a quipu.  Each individual cord represents a single number.
Surprisingly, the Incas used a base-10 positional system, just like we do today.  Each digit of a number 
is represented by a cluster of adjacent knots, with spaces between neighboring clusters.  The digit is 
determined by the number of knots in the cluster.
For example, the number 243 would be represented by a cord with knots tied in the following pattern

-XX-XXXX-XXX-
where each uppercase 'X' represents a knot and each '-' represents an unknotted segment of cord (all quotes for clarity only).

Unlike many ancient civilizations, the Incas were aware of the concept of zero, and used it in their quipus.
A zero is represented by a cluster containing no knots.
For example, the number 204003 would be represented by a cord with knots tied in the following pattern

-XX--XXXX---XXX-
        ^^    ^^^
        ^^    ^^^
        ^^    two zeros between these three segments
        ^^
        one zero between these two segments
Notice how adjacent dashes signal the presence of a zero.

Your task is to translate a single quipu cord into an integer.  The cord will be given as a String *knots*
containing only the characters 'X' and '-'.  There will be a single '-' between each cluster 
of 'X's, as well as a leading '-' and a trailing '-'.  The first cluster will not be empty.

## Definitions
- *Class*: `Quipu`
- *Method*: `readKnots`
- *Parameters*: `String`
- *Returns*: `int`
- *Method signature*: `int readKnots(String knots)`

## Constraints
- *knots* contains between 3 and 50 characters, inclusive.
- *knots* contains only the characters 'X' and '-'.  Note that 'X' is uppercase.
- The first and last characters of *knots* are '-'s.  The second character is 'X'.
- *knots* does not contain 10 consecutive 'X's.
- *knots* will represent a number between 1 and 1000000, inclusive.

## Examples
### Example 1
#### Input
<c>"-XX-XXXX-XXX-"</c>
#### Output
<c>243</c>
#### Reason
The first example above.

### Example 2
#### Input
<c>"-XX--XXXX---XXX-"</c>
#### Output
<c>204003</c>
#### Reason
The second example above.

### Example 3
#### Input
<c>"-X-"</c>
#### Output
<c>1</c>
### Example 4
#### Input
<c>"-X-------"</c>
#### Output
<c>1000000</c>
### Example 5
#### Input
<c>"-XXXXXXXXX--XXXXXXXXX-XXXXXXXXX-XXXXXXX-XXXXXXXXX-"</c>
#### Output
<c>909979</c>

