public class Quipu {

    public int readKnots(String knots){
    	StringBuilder sb=new StringBuilder();
    	char [] ch= new char[knots.length()];
    	knots.getChars(0, knots.length(), ch, 0);
    	int x=0; int minus=0;

    	for (int i=1; i<ch.length; i++){
    		if (ch[i]=='-'){
    		    if (minus>1){
                     	sb.append("0"); minus=1;
               	     }
    		    if (x>0){
    		    	sb.append(String.valueOf(x)); x=0;
    		    }
                    minus++;
    		}


    		if (ch[i]=='X'){ x++; minus=0;

    		}
    	}
    	Integer result=Integer.parseInt(sb.toString());
    	 return result.intValue();
    }
}
